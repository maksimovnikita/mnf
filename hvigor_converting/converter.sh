if [ "$#" -ne 1 ]; then
    echo Enter the path to the directory to convert
    exit 1
fi
xtsPath=$(realpath $1)
echo The folder for converting: $xtsPath

if [ ! -d "$1" ]; then
    echo ERROR Test is not found. Check the path: $xtsPath
    exit 1
fi
if [ ! -d "$xtsPath/entry/src/main/ets" ] && [ ! -d "$xtsPath/src/main/ets" ]; then
    echo ERROR The folder does not exist ets tests
    exit 1
fi
if [ -d "$xtsPath/hvigor" ]; then
    echo ERROR The folder already exist hvigor
    exit 1
fi

describes=$(grep -r "^\s*describe\s*(" $xtsPath | wc -l)
its=$(grep -r "^\s*it\s*(" $xtsPath | wc -l)
printf "\x1b[34mSuites=%d, test cases (it)=%d\n\x1b[0m" $describes $its

echo Start converting...

if [ -d "$xtsPath/src/main/ets" ] && [ ! -d "$xtsPath/entry/src/main/ets" ]; then
    echo Found old structure. Moving src to entry.
    mkdir $xtsPath/entry
    mv -f -v $xtsPath/src $xtsPath/entry
fi

scriptPath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ ! -e "$xtsPath/entry/src/main/ets/Application/AbilityStage.ts" ]; then
    echo Copying AbilityStage.
    cp -r $scriptPath/template2/* $xtsPath/entry/src/main/ets/
fi

echo Copying template files
cp -r $scriptPath/template/* $xtsPath

echo Starting node
cd node
npm i
npm run converting $xtsPath