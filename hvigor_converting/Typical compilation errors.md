1. ВАЖНО, компилятор не выдает ошибку, проверять вручную! Проверить catch блоки. Аргумент в catch следует воспринимать как unknown.  
Например:
```
// Было
try {
...
} catch (error) {
    console.log("code = " + error.code + ", message = " + error.message);
}
// Стало
import { BusinessError } from '@ohos.base';
...
try {
...
} catch (error) {
    console.log("code = " + (error as BusinessError).code + ", message = " + (error as Error).message);
}
```

2. ВАЖНО, компилятор не выдает ошибку, проверять вручную! import должен быть без расширения (удалить .ets)

3. Удалить комментарий @ts-nocheck

4. Автозамена
```
from '@system.router'
from '@ohos.router'
```
```
let options = {
let options: router.RouterOptions = {
```
```
uri:
url:
```
```
function () {
() => {
```
```
function (done) {
(done: Function) => {
```
```
(done)
(done: Function)
```
```
let obj = JSON.parse(
let obj: ESObject = JSON.parse(
```
```
if (eventData.data.
if (eventData.data?.
```
```
if (event.data.
if (event.data?.
```
```
err.message
(err as Error).message
```
```
let callback = (indexEvent) => {
let callback = (indexEvent: events_emitter.EventData) => {
```
```
^(?=.*?\bJSON.parse\b)((?!ESObject|if|this.|console.|expect|stringify|Record<string).)*$
```

5. Явно определить типы вместо неявного any (там где это возможно). Где конкретный тип задать невозможно следует использовать ESObject.  
Например:
```
let eventData: events_emitter.EventData = {
  data: {
    "title": "new title"
  }
}
```
```
let innerEvent: events_emitter.InnerEvent = {
  eventId: 143,
  priority: events_emitter.EventPriority.LOW
}
```
```
private stateChangCallBack = (eventData: events_emitter.EventData) => {
```
```
const abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator()
const abilityDelegatorArguments = AbilityDelegatorRegistry.getArguments()
```
```
// Используй @ohos.router вместо @system.router
let options: router.RouterOptions = {
    url: "MainAbility/pages/UIContext/setFrameRateRange",
}
```
Поиск `^(?=.*?\bJSON.parse\b)((?!ESObject|if|this.|console.|expect|stringify|Record<string).)*$`. Добавить `ESObject`.
```
let obj: ESObject = JSON.parse(strJson);
```

6. Использовать стрелочную функцию вместо function при передаче лямбды.  
Например:
```
function () {
    ...
}
заменить на
() => {
    ...
}
```