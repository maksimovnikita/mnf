import * as fs from 'node:fs'
import { makeAbility } from './ability'
import { makeApp } from './app'
import { makeModule } from './module'
import { replaceNames } from './sed'

converting(process.argv[2])

function converting(xtsFolderPath: string) {
    try {
        if (fs.existsSync(xtsFolderPath)) {
            replaceNames(xtsFolderPath)
            makeApp(xtsFolderPath)
            makeModule(xtsFolderPath)
            makeAbility(xtsFolderPath)
            console.info('\x1b[32m%s\x1b[0m', `The XTS folder ${xtsFolderPath} has been converted.`)
            console.info('\x1b[32m%s\x1b[0m', 'Successfully!')
        }
    } catch (err) {
        console.error('\x1b[31m%s\x1b[0m', `The XTS folder ${xtsFolderPath} has not been converted.`)
        console.error('\x1b[31m%s\x1b[0m', `Error: ${err}`)
    }
}