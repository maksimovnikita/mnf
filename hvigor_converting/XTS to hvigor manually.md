1 Копировать все файлы из папки `template_old`

2 Заменить имена в `Test.json`:  
2.1 Заменить в  `module-name` на `"module-name": "entry_test"`.  
2.2 Добавить в `test-file-name` второй hap (имя первого + Main).  
Например:
```
            "test-file-name": [
                "ActsAceEtsComponentThreeTest.hap",
                "ActsAceEtsComponentThreeTestMain.hap"
            ],
```

3 Заменить имена в `BUILD.gn` `YourHapName` на имя тестового hap (первого) из `Test.json`.

4 Заменить в проекте `your_bundle_name` на `bundle-name` из `Test.json`.

5 `AppScope`  
5.1 В `app.json5` скопировать значение полей из `app.json` (новые не добавлять)  
5.2 Удалить `app.json`  

6 Если есть папка `src` в корне проекта, то нужно переместить ее в папку `entry` и объеденить с `entry/src`.

7 `entry\src\main`  
7.1 Проверить есть ли `ets\Application\AbilityStage.ts`. Если нет, то скопировать из другого каталога.  
7.2 Добавить в `module.json5` информацию из `module.json`  
7.2.1 Следующие поля не менять: `name`, `type`, `srcEntry`, `mainElement`, `abilities.name`, `abilities.srcEntry`, `abilities.startWindowBackground`  
7.2.2 Заменить (синхронизировать) `deviceTypes` кроме `phone` (`phone` устаревший и должен отсутствовать). Дополнительно скопировать `deviceTypes` в `entry\src\ohosTest\module.json5`.  
7.2.3 Скопировать `requestPermissions` (если есть). Изменить имя ability на `EntryAbility`.  
7.2.4 Скопировать значение `abilities.visible` (если есть) в `abilities.exported` (если `visible` нет, то `exported` удалить).  
7.2.5 Скопировать значение `abilities.startWindowIcon` из `module.json` если нет, то взять из `abilities.icon`.  
7.2.6 Синхронизировать/добавить остальные поля. Во всех непонятных случаях создать в DevEco пустой проект, ввести код, посмотреть подсказки.  
Например: `description`, `deliveryWithInstall`, `installationFree`, `pages`, `metadata`, `abilities.description`, `abilities.icon`, `abilities.label`, `abilities.orientation`, `abilities.skills`.  
7.3 Удалить `module.json`  
7.4 Добавить в `ets\entryability\EntryAbility.ets` общий код из `ets\MainAbility\MainAbility.ts` (возможно используется `TestAbility`). Проверить путь к стартовой странице.  
7.5 Удалить `ets\MainAbility\MainAbility.ts` (`TestAbility`)  
7.6 Удалить каталоги  
```
ets\TestRunner
ets\TestAbility (в случае MainAbility)
```
7.7 `resources\base\element\color.json` добавить поле `start_window_background`
```
{
  "color": [
    {
      "name": "start_window_background",
      "value": "#FFFFFF"
    }
  ]
}
```
